package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {
	@Test
	public void testCelsius2() throws Exception {
		Converter converter = new Converter();
		double price = converter.convertToF(2.0);
		Assertions.assertEquals(35.6, price, 0.0, "2 Celsius in Fahrenheit");
	}
}
